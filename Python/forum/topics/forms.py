from django import forms
from forum.tools import models

class TopicForm(forms.ModelForm):
  class Meta:
    model = models.Topic
    fields = ['name']
    
class PostForm(forms.ModelForm):
  class Meta:
    model = models.Post
    fields = ['content']