from django.conf.urls import patterns, url
urlpatterns = patterns('forum.topics.views',
                       url(r'^(?P<pk>\d+)$', 'forumView', name='forum'),
                       url(r'^create/topic/(?P<pk>\d+)$', 'createTopicView', name='createTopic'),
                       url(r'^update/topic/(?P<pk>\d+)$', 'updateTopicView', name='updateTopic'),
                       url(r'^delete/topic/(?P<pk>\d+)$', 'deleteTopicView', name='deleteTopic'),
)
