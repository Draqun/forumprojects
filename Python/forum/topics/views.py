from django.views.generic import ListView, FormView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy 
from forum.tools import models
from forum.topics import forms
from django.shortcuts import render, redirect, get_object_or_404
from braces import views
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin

class ForumView(ListView):
  template_name = "topic_list.html"
  
  def get_queryset(self):
    forum_id = self.kwargs['pk']
    return models.Topic.objects.filter(forum_id=forum_id)
  
  def get_context_data(self, *args, **kwargs):
    context = super().get_context_data(*args, **kwargs)
    context['forum'] = models.Forum.objects.get(id=self.kwargs['pk'])
    return context
  
forumView = ForumView.as_view()

class UpdateTopicView(views.LoginRequiredMixin, views.StaffuserRequiredMixin, SuccessMessageMixin, UpdateView):
  template_name = "form_base.html"
  model = models.Topic
  form_class = forms.TopicForm
  success_message = "Pomyśle zaktualizowano temat."
  
  def get_success_url(self):
    forum_pk = int(models.Topic.objects.get(id=self.kwargs['pk']).forum.pk)
    return reverse_lazy('forum', kwargs={ 'pk': forum_pk })

updateTopicView = UpdateTopicView.as_view()

class DeleteTopicView(views.LoginRequiredMixin, views.StaffuserRequiredMixin, SuccessMessageMixin, DeleteView):
  template_name = "tools/forum_confirm_delete.html"
  model = models.Topic
  success_url = reverse_lazy('index')
  success_message = "Pomyśle usunięto temat."
  
  def get_success_url(self):
    forum_pk = int(models.Topic.objects.get(id=self.kwargs['pk']).forum.pk)
    return reverse_lazy('forum', kwargs={ 'pk': forum_pk })

deleteTopicView = DeleteTopicView.as_view()

class CreateTopicView(views.LoginRequiredMixin, views.PermissionRequiredMixin, SuccessMessageMixin, FormView):
  template_name = "create_topic.html"
  permission_required = 'add_topic'
  model = models.Topic
  form_class = forms.TopicForm
  success_message = "Pomyślnie stworzono temat"
  success_url = reverse_lazy('index')
  
  def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    context['form2'] = forms.PostForm()
    context['forum'] = models.Forum.objects.get(id=self.kwargs['pk'])
    return context
  
  def post(self, request, *args, **kwargs):
    response = super().post(request, *args, **kwargs)
    form = self.form_class({'name': request.POST['name']})
    form2 = forms.PostForm({'content': request.POST['content']})
    
    if form.is_valid() and form2.is_valid():
      messages.success(request, "Pomyślnie stworzono temat.")
      topic = self.model(name = request.POST['name'], author = request.user, forum = models.Forum.objects.get(id=kwargs['pk']))
      topic.save()
      post = models.Post(content = request.POST['content'], author = request.user, topic = topic)
      post.save()
      return redirect(reverse_lazy('posts', kwargs={'pk': topic.pk}))
    else:
      return render(request, self.template_name, {'form': form, 'form2': form2})
  
createTopicView = CreateTopicView.as_view()
 