from django.views.generic import FormView, UpdateView, DeleteView
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy
from forum.tools import models
from forum.topics import forms
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from braces import views

class PostsView(FormView):
  template_name = "posts.html"
  model = models.Post
  form_class = forms.PostForm
  success_url = reverse_lazy('index')
  
  def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    topic = get_object_or_404(models.Topic, pk=self.kwargs['pk'])
    context['post_list'] = self.model.objects.filter(topic = topic)
    context['forum'] = topic.forum
    if not 'tools.add_post' in self.request.user.get_all_permissions():
      context['form'] = None
    return context
  
  def post(self, request, *args, **kwargs):
    response = super().post(request, *args, **kwargs)
    form = self.form_class({'content': request.POST['content']})
    
    if request.user.is_authenticated() and form.is_valid():
      topic = get_object_or_404(models.Topic, pk=kwargs['pk'])
      post = models.Post(content = request.POST['content'], author = request.user, topic = topic)
      post.save()
      messages.success(request, "Pomyślnie dodano wiadomość.")
      return redirect(reverse_lazy('posts', kwargs={'pk': topic.pk}))
    else:
      return render(request, self.template_name, {'form': form })
  
postsView = PostsView.as_view()

class UpdatePostView(views.LoginRequiredMixin, views.UserPassesTestMixin, SuccessMessageMixin, UpdateView):
  template_name = "posts.html"
  model = models.Post
  form_class = forms.PostForm
  success_message = "Pomyślnie zaktualizowano wiadomość."
  
  def get_success_url(self):
    topic_pk = int(models.Post.objects.get(id=self.kwargs['pk']).topic.pk)
    return reverse_lazy('posts', kwargs={ 'pk': topic_pk })
  
  def test_func(self, user):
    post = models.Post.objects.get(id=self.kwargs['pk'])
    return user.is_staff or user.pk == post.author.pk
  
  def get_context_data(self, **kwargs):
    context = super().get_context_data(**kwargs)
    post = context['post']
    context['forum'] = post.topic.forum
    context['post_list'] = models.Post.objects.filter(topic_id = post.topic.pk) 
    return context
  
updatePostView = UpdatePostView.as_view()

class DeletePostView(views.LoginRequiredMixin, views.StaffuserRequiredMixin, SuccessMessageMixin, DeleteView):
  template_name = "tools/forum_confirm_delete.html"
  model = models.Post
  success_message = "Pomyślnie usunięto post."
  
  def get_success_url(self):
    topic_pk = int(models.Post.objects.get(id=self.kwargs['pk']).topic.pk)
    return reverse_lazy('posts', kwargs={ 'pk': topic_pk })
  
deletePostView = DeletePostView.as_view()
