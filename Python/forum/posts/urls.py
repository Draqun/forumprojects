from django.conf.urls import patterns, url
urlpatterns = patterns('forum.posts.views',
                       url(r'^(?P<pk>\d+)$', 'postsView', name='posts'),
                       url(r'^update/(?P<pk>\d+)$', 'updatePostView', name="updatePost"),
                       url(r'^delete/(?P<pk>\d+)$', 'deletePostView', name="deletePost")
)
