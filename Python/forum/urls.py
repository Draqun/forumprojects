from django.conf.urls import patterns, include, url
from django.contrib.auth.views import logout, login

urlpatterns = patterns('',
    url(r'^', include('forum.tools.urls')),
    url(r'^forum/', include('forum.topics.urls')),
    url(r'^topic/', include('forum.posts.urls')),
    url(r'^login$', login, name='login'),
    url(r'^logout$', logout, {'next_page':'index'}, name='logout')
)
