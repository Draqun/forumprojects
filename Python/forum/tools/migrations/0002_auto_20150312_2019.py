# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='forum',
            options={'ordering': ['order_nr']},
        ),
        migrations.AlterModelOptions(
            name='post',
            options={'ordering': ['created_at']},
        ),
        migrations.AlterModelOptions(
            name='topic',
            options={'ordering': ['-id']},
        ),
        migrations.AlterField(
            model_name='forum',
            name='name',
            field=models.CharField(max_length=64, unique=True, validators=[django.core.validators.MinLengthValidator(4)]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='forum',
            name='order_nr',
            field=models.IntegerField(validators=[django.core.validators.MinValueValidator(0)], unique=True),
            preserve_default=True,
        ),
    ]
