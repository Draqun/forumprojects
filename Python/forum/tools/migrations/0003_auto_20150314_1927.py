# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
import django.core.validators
import forum.tools.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0002_auto_20150312_2019'),
    ]

    operations = [
        migrations.AlterField(
            model_name='forum',
            name='comment',
            field=models.CharField(validators=[django.core.validators.MinLengthValidator(4)], max_length=64, verbose_name='Opis'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='forum',
            name='name',
            field=models.CharField(validators=[django.core.validators.MinLengthValidator(4)], unique=True, max_length=64, verbose_name='Nazwa'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='forum',
            name='order_nr',
            field=models.IntegerField(validators=[django.core.validators.MinValueValidator(0)], unique=True, verbose_name='Liczba porządkowa'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='author',
            field=models.ForeignKey(verbose_name='Autor', to=settings.AUTH_USER_MODEL, on_delete=models.SET(forum.tools.models.get_or_create_deleted)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='content',
            field=models.TextField(validators=[django.core.validators.MinLengthValidator(10)], verbose_name='Wiadomość'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='topic',
            field=models.ForeignKey(verbose_name='Temat', to='tools.Topic'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='topic',
            name='author',
            field=models.ForeignKey(verbose_name='Autor', to=settings.AUTH_USER_MODEL, on_delete=models.SET(forum.tools.models.get_or_create_deleted)),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='topic',
            name='forum',
            field=models.ForeignKey(verbose_name='Forum', to='tools.Forum', on_delete=django.db.models.deletion.DO_NOTHING),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='topic',
            name='name',
            field=models.CharField(validators=[django.core.validators.MinLengthValidator(4)], max_length=64, verbose_name='Nazwa'),
            preserve_default=True,
        ),
    ]
