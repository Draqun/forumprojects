from forum.tools import models
from django import forms

class ForumForm(forms.ModelForm):
  class Meta:
    model = models.Forum
    fields = ['name', 'comment', 'order_nr']