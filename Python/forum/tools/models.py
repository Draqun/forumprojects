from django.db import models
from django.core.validators import MinValueValidator, MinLengthValidator
from django.contrib.auth.models import User

def get_or_create_deleted():
  return User.objects.get_or_create(name="Usunięty")[0]

class Forum(models.Model):
  name = models.CharField(max_length=64, validators=[MinLengthValidator(4)], null=False, blank=False, unique=True, verbose_name="Nazwa")
  comment = models.CharField(max_length=64, validators=[MinLengthValidator(4)], null=False, blank=False, verbose_name="Opis")
  order_nr = models.IntegerField(validators=[MinValueValidator(0)], unique=True, verbose_name="Liczba porządkowa")

  def __str__(self):
    return self.name

  class Meta:
    ordering = ['order_nr']

class Topic(models.Model):
  name = models.CharField(max_length=64, validators=[MinLengthValidator(4)], verbose_name="Nazwa")
  created_at = models.DateTimeField(auto_now=True, editable=False, null=False, blank=False)
  forum = models.ForeignKey('Forum', null=False, blank=False, verbose_name="Forum", on_delete = models.DO_NOTHING)
  author = models.ForeignKey(User, null=False, blank=False, verbose_name="Autor", on_delete = models.SET(get_or_create_deleted))

  def __str__(self):
    return self.name

  class Meta:
    ordering = ['-id']

class Post(models.Model):
  author = models.ForeignKey(User, null=False, blank=False, verbose_name="Autor", on_delete = models.SET(get_or_create_deleted))
  content = models.TextField(validators=[MinLengthValidator(10)], null=False, blank=False, verbose_name="Wiadomość")
  created_at = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)
  topic = models.ForeignKey('Topic', null=False, blank=False, verbose_name="Temat")

  def __str__(self):
    return self.content[:10]

  class Meta:
    ordering = ['created_at']
