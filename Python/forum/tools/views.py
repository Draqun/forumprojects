from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from forum.tools import models, forms
from django.core.urlresolvers import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from braces import views
from django.contrib.auth import models as auth_model, forms as auth_forms
from django.db import transaction
from django.db.utils import IntegrityError
from django.contrib import messages
from django.shortcuts import redirect

class IndexView(ListView):
  model = models.Forum
  
indexView = IndexView.as_view()


class CreateForum(views.LoginRequiredMixin, views.StaffuserRequiredMixin, SuccessMessageMixin, CreateView):
  template_name = "form_base.html"
  model = models.Forum
  form_class = forms.ForumForm
  success_url = reverse_lazy('index')
  success_message = "Pomyślnie dodano forum."
  
createForum = CreateForum.as_view()

class UpdateForum(views.LoginRequiredMixin, views.StaffuserRequiredMixin, SuccessMessageMixin, UpdateView):
  template_name  = "form_base.html"
  model = models.Forum
  form_class = forms.ForumForm
  success_url = reverse_lazy('index')
  success_message = "Pomyślnie zaktualizowano forum."
  
updateForum = UpdateForum.as_view()

class DeleteForum(views.LoginRequiredMixin, views.StaffuserRequiredMixin, SuccessMessageMixin, DeleteView):
  model = models.Forum
  success_url = reverse_lazy('index')
  success_message = "Pomyślnie usunięto forum."
  
  def post(self, request, *args, **kwargs):
    try:
      return super().post(request, *args, **kwargs)
    except IntegrityError:
      messages.error(request, "Nie możesz usunąć forum ponieważ nie jest ono puste!")
    return redirect(reverse_lazy('index'))
        
  
deleteForum = DeleteForum.as_view()

class RegisterView(SuccessMessageMixin, CreateView):
  template_name="form_base.html"
  model = auth_model.User
  form_class = auth_forms.UserCreationForm
  success_url = reverse_lazy("login")
  success_message = "Pomyślnie stworzono konto."
  
  @transaction.atomic
  def post(self, request, *args, **kwargs):
    response = super().post(request, *args, **kwargs)
    if response.status_code == 302:
      user_group, created = auth_model.Group.objects.get_or_create(name = 'Users')
      user = auth_model.User.objects.latest('id')
      user.groups.add(user_group)
    return response
  
registerView = RegisterView.as_view()