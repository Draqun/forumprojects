from django.conf.urls import patterns, url
urlpatterns = patterns('forum.tools.views',
                       url(r'^$', 'indexView', name='index'),
                       url(r'^index$', 'indexView', name='index'),
                       url(r'^create/forum$', 'createForum', name='createForum'),
                       url(r'^update/forum/(?P<pk>\d+)$', 'updateForum', name='updateForum'),
                       url(r'^delete/forum/(?P<pk>\d+)$', 'deleteForum', name='deleteForum'),
                       url(r'^register$', 'registerView', name='register')
)
