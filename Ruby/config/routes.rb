Rails.application.routes.draw do
  root 'forums#index'

  resources :users, only: [:new, :create]

  resources :posts, only: [:destroy, :edit, :update]
  get 'posts/:id/new' => 'posts#topic_posts', as: 'new_post'
  post 'posts/:id/new' => 'posts#create'
  get 'topic_posts/:id' => 'posts#topic_posts', as: 'topic_posts'

  resources :topics, only: [:destroy, :edit, :update]
  get 'topics/:id/new' => 'topics#new', as: 'new_topic'
  post 'topics/:id/new' => 'topics#create'
  get 'forum_topics/:id' => 'topics#forum_topics', as: 'forum_topics'

  resources :forums, only: [:new, :edit, :update, :create, :destroy, :index]
  resources :sessions, only: [:new, :create, :destroy, :index]

  get 'login' => 'sessions#new'
  delete 'logout' => 'sessions#destroy'
end
