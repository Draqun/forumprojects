json.array!(@forums) do |forum|
  json.extract! forum, :id, :name, :comment, :order_nr
  json.url forum_url(forum, format: :json)
end
