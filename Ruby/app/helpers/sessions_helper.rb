module SessionsHelper
  def sign_in(user)
    remember_token = User.new_remember_token
    cookies.permanent[:remember_token] = { value: remember_token,
                                           expires: 1.hour.from_now.utc }
    user.update_attribute(:remember_token, User.digest(remember_token))
    self.current_user = user
    cookies[:follow_after_login]
  end

  def sign_out()
    current_user.update_attribute(:remember_token, User.digest(User.new_remember_token))
    cookies.delete(:remember_token)
    cookies.delete(:calendar_date)
    self.current_user = nil
    cookies.delete(:follow_after_login) if cookies[:follow_after_login]
    root_path
  end

  def signed_in?
    if self.current_user.nil?
      unless request.fullpath.include?(login_path) || request.fullpath.include?(logout_path) || request.fullpath.include?(sessions_path)
        cookies[:follow_after_login] = request.fullpath
      end
      false
    else
      true
    end
  end

  def signed_in_user
    redirect_to login_url unless signed_in?
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    remember_token = User.digest(cookies[:remember_token])
    @current_user ||= User.find_by(remember_token: remember_token)
  end

  def current_user?(user)
    user == current_user
  end
end