module ApplicationHelper
  def is_active?(url)
    if url != root_path
      request.original_url.include?(url)
    elsif request.original_url == root_url && url == root_path || url == ""
      request.original_url.include?(root_path)
    end
  end
end
