class ForumsController < ApplicationController
  before_action :signed_in_user, only: [:new, :edit, :update, :destroy]
  before_action :has_admin_permission, only: [:new, :create, :edit, :update, :destroy]

  expose :forums
  expose :forum, attributes: :forum_params

  # POST /forums
  # POST /forums.json
  def create
    respond_to do |format|
      if forum.save
        format.html { redirect_to root_path, notice: 'Forum was successfully created.' }
        format.json { render :index, status: :created, location: forum }
      else
        format.html { render :new }
        format.json { render json: forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forums/1
  # PATCH/PUT /forums/1.json
  def update
    respond_to do |format|
      if forum.save
        format.html { redirect_to root_path, notice: 'Forum was successfully updated.' }
        format.json { render :index, status: :ok, location: forum }
      else
        format.html { render :edit }
        format.json { render json: forum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forums/1
  # DELETE /forums/1.json
  def destroy
    forum.destroy
    respond_to do |format|
      format.html { redirect_to forums_url, notice: 'Forum was successfully destroyed.' }
      format.json { head :no_content }
    end
  rescue ActiveRecord::DeleteRestrictionError
    respond_to do |format|
      format.html { redirect_to forums_url, notice: 'Forum canot be destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_params
      params.require(:forum).permit(:name, :comment, :order_nr)
    end
end
