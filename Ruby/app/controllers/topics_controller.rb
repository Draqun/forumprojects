class TopicsController < ApplicationController
  before_action :signed_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_topic, only: [:edit, :update, :destroy]

  def new
    @topic = Topic.new forum_id: params[:id]
    @topic.posts.build
  end

  def edit
  end

  # POST /topics
  # POST /topics.json
  def create
    @topic = Topic.new name: params[:topic][:name], forum_id: params[:id], user_id: current_user.id
    @topic.posts.new content: params[:topic][:posts_attributes]["0"][:content], user_id: current_user.id

    @topic.transaction do
      respond_to do |format|
        if @topic.save
          format.html { redirect_to forum_topics_path(params[:id]), notice: 'Topic was successfully created.' }
        else
          format.html { render :new }
        end
      end
    end
  end

  # PATCH/PUT /topics/1
  # PATCH/PUT /topics/1.json
  def update
    respond_to do |format|
      topic_params[:user_id] = current_user.id
      if @topic.update(topic_params)
        format.html { redirect_to forum_topics_path(@topic.forum_id), notice: 'Topic was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    forum_id = @topic.forum_id
    @topic.destroy
    respond_to do |format|
      format.html { redirect_to forum_topics_path(forum_id), notice: 'Topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def forum_topics
    @topic_list = Topic.where forum_id: params[:id]
    @forum = Forum.find(params[:id])
  end

  private
    def set_topic
      @topic = Topic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_params
      params.require(:topic).permit(:name, [:content])
    end
end
