class SessionsController < ApplicationController
  before_action :signed_in_user, only: [:destroy]

  def new
    if signed_in?
      redirect_to root_path
    else
      @user = User.new
    end
  end

  def destroy
    redirect_to sign_out, notice: "You are successfully logged out!"
  end

  def index
    redirect_to root_path
  end

  def create
    user = User.find_by(login: params[:session][:login], active:true)
    if user && user.authenticate(params[:session][:password])
      redirect_to sign_in(user)
    else
      unless User.find_by(login: params[:session][:login], active:false)
        flash[:warning]="Invalid login or invalid password!"
      else
        flash[:danger]="Account is not active!"
      end
      @error = true
      render 'new'
    end
  end
end