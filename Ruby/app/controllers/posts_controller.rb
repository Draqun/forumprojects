class PostsController < ApplicationController
  before_action :signed_in_user, only: [:create, :edit, :update, :destroy]
  before_action :set_post, only: [:edit, :destroy, :update]
  before_action :set_view_content, only: [:create, :topic_posts]
  before_action :set_view_content2, only: [:edit, :update]

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = current_user.posts.new content: params[:post][:content], topic_id: params[:id]

    respond_to do |format|
      if @post.save
        format.html { redirect_to topic_posts_path(@post.topic_id), notice: 'Post was successfully created.' }
      else
        format.html { render :topic_posts }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to topic_posts_path(@post.topic_id), notice: 'Post was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
  topic_id = @post.topic_id
    @post.destroy
    respond_to do |format|
      format.html { redirect_to topic_posts_path(topic_id), notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def topic_posts
    @post = Post.new topic_id: params[:id]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:user_id, :content, :topic_id)
    end

    def set_view_content
      @posts = Post.where topic_id: params[:id]
      @forum = Topic.find(params[:id]).forum
    end

    def set_view_content2
      @posts = @post.topic.posts
      @forum = @post.topic.forum
    end
end
