class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  include ApplicationHelper
  after_action :set_label_success, only: [:create, :update]
  after_action :reset_label
  before_action :has_admin_permission, only: [:destroy]
  before_action :check_update_permissions, only: [:edit, :update]

  decent_configuration do
    strategy DecentExposure::StrongParametersStrategy
  end

  def set_label_success
    cookies[:notice_type] = 'success'
  end

  def reset_label
    cookies.delete(:notice_type)
  end

  def has_admin_permission
    unless current_user.admin ||  params[:action] == "destroy" && params[:controller] == "sessions"
      cookies[:notice_type] = 'danger'
      respond_to do |format|
        format.html { redirect_to root_path, notice: "You don't have permission to this action." }
      end
    end
  end

  def check_update_permissions
    unless current_user.admin || params[:controller] == 'posts' && ( params[:action] == 'edit' || params[:action] == 'update' ) && current_user.id == Post.find(params[:id]).user_id
      cookies[:notice_type] = 'danger'
      respond_to do |format|
        format.html { redirect_to root_path, notice: "You don't have permission to this action." }
      end
    end
  end
end
