class UsersController < ApplicationController
  expose :user, attributes: :user_params

  # POST /users
  # POST /users.json
  def create
    respond_to do |format|
      if user.save
        format.html { redirect_to root_path, notice: 'User was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:login, :email, :password_digest, :password, :password_confirmation)
    end
end
