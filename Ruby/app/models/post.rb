class Post < ActiveRecord::Base
  belongs_to :topic, :inverse_of => :posts
  belongs_to :user

  #validates :topic, presence: true
  validates :user, presence: true

  validates :content, length: { minimum: 10 }

  default_scope -> { order(created_at: :asc) }
end
