class Forum < ActiveRecord::Base
  has_many :topics, dependent: :restrict_with_exception

  validates :order_nr, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :name, presence: true, length: { minimum: 4, maximum: 64 }
  validates :comment, presence: true, length: { minimum: 4, maximum: 64 }

  default_scope -> { order(order_nr: :asc) }

  validates_uniqueness_of :order_nr
  validates_uniqueness_of :name
end
