class User < ActiveRecord::Base
  has_secure_password
  has_many :posts, dependent: :restrict_with_error
  has_many :topics, dependent: :restrict_with_error

  validates :login, presence: true, length: { maximum: 30, minimum: 3 }

  validates_uniqueness_of :login

  before_create :create_remember_token

  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private
    def create_remember_token
      self.remember_token = User.digest(User.new_remember_token)
    end
end
