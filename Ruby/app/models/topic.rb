class Topic < ActiveRecord::Base
  belongs_to :forum
  belongs_to :user
  has_many :posts, dependent: :destroy

  validates :forum, presence: true
  validates :user, presence: true

  validates :name, length: { minimum: 4, maximum: 64 }

  default_scope -> { order(created_at: :desc) }

  accepts_nested_attributes_for :posts
end
