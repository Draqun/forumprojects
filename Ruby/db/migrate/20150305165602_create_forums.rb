class CreateForums < ActiveRecord::Migration
  def change
    create_table :forums do |t|
      t.string :name, null: false
      t.text :comment, null: false
      t.integer :order_nr, null: false, unique: true

      t.timestamps
    end
  end
end
