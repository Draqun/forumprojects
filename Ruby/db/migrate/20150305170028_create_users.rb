class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :login, unique: true
      t.string :email, null: true
      t.string :password_digest, null: true
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
