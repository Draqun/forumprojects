class AddAuthorToTopic < ActiveRecord::Migration
  def change
    add_column :topics, :user_id, :integer, null: false
  end
end
