class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :name, null: false
      t.integer :forum_id, null: false

      t.timestamps
    end
  end
end
