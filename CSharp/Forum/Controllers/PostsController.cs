﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Forum.Models;

namespace Forum.Controllers
{
    public class PostsController : Controller
    {
        private PostsContext db = new PostsContext();

        //
        // GET: /Posts/

        public ActionResult Index(int TopicId)
        {
            var posts = db.Posts.Include(p => p.Topic).Where(p => p.TopicId == TopicId);
            var topic = db.Topics.Find(TopicId);
            ViewBag.TopicId = TopicId;
            ViewBag.TopicName = topic.Name;
            ViewBag.CategoryId = topic.CategoryId;
            return View(posts.ToList());
        }

        //
        // GET: /Posts/Create
        [Authorize]
        public ActionResult Create(int TopicId)
        {
            ViewBag.TopicId = TopicId;
            return View();
        }

        //
        // POST: /Posts/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create(Post post, int TopicId)
        {
            if (ModelState.IsValidField("Context"))
            {
                post.Author = User.Identity.Name;
                post.TopicId = TopicId;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index", new { TopicId = TopicId });
            }

            ViewBag.TopicId = TopicId;
            return View(post);
        }

        //
        // GET: /Posts/Edit/5
        [Authorize]
        public ActionResult Edit(int id, int TopicId)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.TopicId = TopicId;
            return View(post);
        }

        //
        // POST: /Posts/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(Post post, int TopicId)
        {
            if (ModelState.IsValidField("Context"))
            {
                post.Author = User.Identity.Name;
                post.TopicId = TopicId;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { TopicId = TopicId });
            }
            ViewBag.TopicId = TopicId;
            return View(post);
        }

        //
        // GET: /Posts/Delete/5
        [Authorize(Roles="Admin")]
        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.TopicId = post.TopicId;
            return View(post);
        }

        //
        // POST: /Posts/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id, int TopicId)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index", new { TopicId = TopicId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}