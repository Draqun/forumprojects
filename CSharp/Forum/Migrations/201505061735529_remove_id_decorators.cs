namespace Forum.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remove_id_decorators : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Topics", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Posts", "Id", c => c.Int(nullable: false, identity: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Topics", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Categories", "Id", c => c.Int(nullable: false));
        }
    }
}
